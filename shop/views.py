# -*- coding: utf-8 -*-
import datetime
import re, sys, os
import random
import requests
import json
import csv
import vk
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse
from django.http.response import Http404
from django.core.mail import send_mail, BadHeaderError
from django.template import RequestContext, loader
from django.db.models import Q
from django.template.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import redirect

# импортируем модели
from .models import Textpage, Category, Subcategory, Item, Client, Order, PayMethod, ClientsAndOrders, Scripts, Robots, CommentAuths, CommentVk

def active_menu(req):
    alias = req.path.strip('/').split('/')[0]
    if alias == '':
        alias = 'index'

    return alias
    
def default_context(request,alias,object):
    menu = Textpage.objects.order_by('menuposition').filter(~Q(alias="index"),menushow=True)
    
    data = get_object_or_404(object, alias=alias, host__name=request.get_host())
    scripts = Scripts.objects.all()
    
    if request.get_host() == '176.57.217.200':
        template_prefix = 'getshop'
    elif request.get_host() == 'royal-accs.net':
        template_prefix = 'getshop'
    elif request.get_host() == 'sweet-acc.ru':
        template_prefix = 'main'
        
    
    context_object = {
        'menu' : menu,
        'data' : data,
        'scripts' : scripts,
        'template_prefix' : template_prefix,
    }
    
    return context_object
    
def comments(request):
    context_data = default_context( request , 'reviews' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'comments.html')
    
    if request.GET.get('code'):
        code = request.GET['code']
        first = requests.get('https://oauth.vk.com/access_token?client_id=6350467&client_secret=1IPjkPuSxUm1xl5mXJu6&redirect_uri=http://royal-accs.net/reviews&code=' + code).json()
        if first['access_token']:
            access_token = first['access_token']
            uid = str(first['user_id'])
            fields = 'uid,first_name,last_name,photo_big'
            second = requests.get('https://api.vk.com/method/users.get?user_id=' + uid + '&fields=' + fields + '&access_token=' + access_token).json()
            userdata = dict(
                name=second['response'][0]['first_name'],
                surename=second['response'][0]['last_name'],
                uid=second['response'][0]['uid'],
                photo_big=second['response'][0]['photo_big']
            )
    else:
        userdata = ''
        
    if userdata == '':
        return redirect('https://oauth.vk.com/authorize?client_id=6350467&redirect_uri=http://royal-accs.net/reviews&response_type=code')
    
    com = CommentVk.objects.all().exclude(~Q(vkid=userdata['uid']), view=False).order_by('-id')
    
    paginator = Paginator(com, 20)
    page = request.GET.get('page')
    
    try:
        comments = paginator.page(page)
    except PageNotAnInteger:
        comments = paginator.page(1)
    except EmptyPage:
        comments = paginator.page(paginator.num_pages)
        
    count = CommentVk.objects.filter(view=True).count()
    context_data.update({
	    'userdata' : userdata,
        'count' : count,
        'comments' : comments,
    })

    return HttpResponse(template.render(context_data))
    
def soc_auth(request):

    if request.GET.get('code'):
        code = request.GET['code']
        first = requests.get('https://oauth.vk.com/access_token?client_id=6350467&client_secret=1IPjkPuSxUm1xl5mXJu6&redirect_uri=http://royal-accs.net/auth&code=' + code).json()
        if first['access_token']:
            access_token = first['access_token']
            uid = str(first['user_id'])
            fields = 'uid,first_name,last_name,photo_big'
            second = requests.get('https://api.vk.com/method/users.get?user_id=' + uid + '&fields=' + fields + '&access_token=' + access_token).json()
            userdata = dict(
                name=second['response'][0]['first_name'],
                surename=second['response'][0]['last_name'],
                uid=second['response'][0]['uid'],
                photo_big=second['response'][0]['photo_big']
            )

def index(request):

    context_data = default_context( request , "index" , Textpage )
    
    template = loader.get_template(context_data['template_prefix'] + '/' + 'index.html')
    
    c = {}
    c.update(csrf(request))
    
    table = Item.objects.filter(host__name=request.get_host()).order_by('menuposition')
    if request.GET.get('price'):
        get_price = request.GET['price']
        if request.GET['price'] == '300':
            table = Item.objects.filter(host__name=request.get_host(), price__lte=300).order_by('menuposition')
        elif request.GET['price'] == '300-500':
            table = Item.objects.filter(host__name=request.get_host(), price__gte=300, price__lte=500).order_by('menuposition')
        elif request.GET['price'] == '500':
            table = Item.objects.filter(host__name=request.get_host(), price__gte=500).order_by('menuposition')
    else:
        get_price = False
        
    paginator = Paginator(table, 15)
    page = request.GET.get('page')
    
    slider = []
    
    i = 0
    while i < 5:
        slider.append([Item.objects.exclude(Q(slider_big='') | Q(slider_big__exact=None)).order_by('slider_position')[i:i+1],Item.objects.exclude(Q(slider_small='') | Q(slider_small__exact=None)).order_by('slider_position')[i*2:(i+1)*2]])
        i+=1
    
    try:
        accss = paginator.page(page)
    except PageNotAnInteger:
        accss = paginator.page(1)
    except EmptyPage:
        accss = paginator.page(paginator.num_pages)

    context_data.update({
        'slider': slider,
        'accss' : accss,
        'c': c,
        'get_price': get_price,
    })

    return HttpResponse(template.render(context_data))

def agreement(request):
    context_data = default_context( request , 'agreement' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'soglashenie.html')
    
    return HttpResponse(template.render(context_data))
    
def activate(request):
    context_data = default_context( request , 'aktivaciya-klyucha-v-steam' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'soglashenie.html')
    
    return HttpResponse(template.render(context_data))
    
def activate_o(request):
    context_data = default_context( request , 'aktivaciya-klyucha-v-origin' , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'soglashenie.html')
    
    return HttpResponse(template.render(context_data))
    
def robots(request):
    template = loader.get_template('robots.txt')
    context = Robots.objects.filter(host__name=request.get_host())
    data = {'context':context}

    return HttpResponse(template.render(data), content_type="text/plain")
    
def get_fields(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        drugs = Item.objects.filter(name__icontains = q )[:8]
        results = []
        for drug in drugs:
            drug_json = {}
            drug_json['img'] = drug.pic.url
            drug_json['label'] = drug.name
            drug_json['value'] = drug.alias
            drug_json['price'] = drug.price
            drug_json['category'] = drug.category.alias
            results.append(drug_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)
    
def textpage(request, alias):
    context_data = default_context( request , alias , Textpage )
    template = loader.get_template(context_data['template_prefix'] + '/' + 'textpage.html')
 
    context_data.update({

    })

    return HttpResponse(template.render(context_data))

def clientspage(request):
    context_data = default_context(request, "clients", Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'clients.html')
    items = ClientsAndOrders.objects.all()

    context_data.update({
        'clients' : items,
    })

    return HttpResponse(template.render(context_data))

def product(request, alias, category_alias):
    context_data = default_context(request, alias, Item)
    get_object_or_404(Category, alias=category_alias)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'product.html')
        
    rand_num = str(random.randint(1000000, 9999999))
    
    context_data.update({
        'rand_num' : rand_num,
    })

    return HttpResponse(template.render(context_data))
    
def test(request):
    context_data = default_context(request, 'test', Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'test.html')
    
    session = vk.Session(access_token='4fb22c0b4fb22c0b4fb22c0bb84fd2ca8844fb24fb22c0b15dc2e8b7ca03d826e83497d')
    vk_api = vk.API(session)
    test = vk_api.users.get(user_id=1, fields="first_name,last_name,photo_big, uid")
    
    context_data.update({
        'test' : test,
    })

    return HttpResponse(template.render(context_data))

def categories(request):
    context_data = default_context(request, "categories", Textpage)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'categories.html')
    categories = Category.objects.filter(host__name=request.get_host())

    context_data.update({ 'categories': categories, })

    return HttpResponse(template.render(context_data))

def category(request, alias):
    context_data = default_context(request, alias, Category)
    template = loader.get_template(context_data['template_prefix'] + '/' + 'category.html')
    items = Item.objects.filter(category__alias=alias).order_by('menuposition')
    if request.GET.get('price'):
        if request.GET['price'] == '300':
            items = Item.objects.filter(category__alias=alias, price__lte=300).order_by('menuposition')
        elif request.GET['price'] == '300-500':
            items = Item.objects.filter(category__alias=alias, price__gte=300, price__lte=500).order_by('menuposition')
        elif request.GET['price'] == '500':
            items = Item.objects.filter(category__alias=alias, price__gte=500).order_by('menuposition')

    context_data.update({ 
        'items': items,
    })

    return HttpResponse(template.render(context_data))    
    
def check_order(token):
    api_access_token = 'e3a2a373dfe2bfdebf5aa83550d7198f' 
    my_login = '+79252757855'

    s = requests.Session()
    s.headers['authorization'] = 'Bearer ' + api_access_token
    parameters = {'rows': '1'}
    h = s.get('https://edge.qiwi.com/payment-history/v1/persons/'+my_login+'/payments', params = parameters)
    a = json.loads(h.text)
    for i in a['data']:
        if token == i['comment']:
            order_item = Order.objects.get(hash=token)
            if order_item.status != u'Выдан':
                if (i['sum'])['amount'] >= order_item.price:
                    counter = order_item.count
                    items = order_item.item.items.split('\r\n')
                    otdat_pokupatelyu = ''
                    vernut_nazad = ''
                    for items_item in items:
                        if counter > 0:
                            otdat_pokupatelyu += items_item+'\r\n'
                            counter = counter - 1
                        else:
                            vernut_nazad += items_item+'\r\n'
                    Item.objects.filter(id=order_item.item.id).update(items=vernut_nazad, count=(order_item.item.count-order_item.count))
                    order_item.status = u'Выдан'
                    order_item.items = otdat_pokupatelyu
                    order_item.save()
                    return otdat_pokupatelyu
            else:
                return order_item.items
        else:
            return 'Неоплачено'
            
def ajax(request):
    if request.is_ajax():
        # create_task
        if request.POST['type'] == 'create_order':
            client, created = Client.objects.get_or_create(mail=request.POST['mail'])
            order, created = Order.objects.get_or_create(order_id=666,paymethod=PayMethod.objects.get(id=request.POST['paymethod']),
                                client=client, item=Item.objects.get(id=request.POST['item_id']), count=request.POST['count'],
                                price=request.POST['total_price'], status='Заказ не оплачен', hash='zakaz' + str(random.randint(1000000, 9999999)))
            MyValue = order.hash
        elif request.POST['type'] == 'check_order':
            a = check_order(request.POST['token'])
            MyValue = a
            
        elif request.POST['type'] == 'add_comment':
            if CommentVk.objects.filter(vkid=request.POST['data[vk_id]']).count() > 3:
                MyValue = '234'
            else:
                item = CommentVk(vkid=request.POST['data[vk_id]'], comment=request.POST['data[text]'], avatar=request.POST['data[img]'], username=request.POST['data[username]'], time=request.POST['data[time]'])
                item.save()
                MyValue = '123'
    return HttpResponse(MyValue)