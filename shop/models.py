# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from unidecode import unidecode
from django.template import defaultfilters 
from django.contrib.auth.models import User, Group
from django.utils import timezone
from django.utils.dateformat import format
from django.conf import settings
from django.contrib.sites.models import Site

import datetime
import json
import uuid
import re
import math

from django.db import models

class Page(models.Model):
    class Meta:
        abstract = True

    hash = models.CharField( max_length=200 , verbose_name=u"Хэш" )
    alias = models.SlugField(max_length=200, verbose_name=u"Псевдоним")
    content = models.TextField( verbose_name=u"Статья" , null=True , blank=True )
    seo_h1 = models.CharField( max_length=200 , verbose_name="H1" , null=True , blank=True )
    seo_title = models.CharField( max_length=200 , verbose_name="Title" , null=True , blank=True )
    seo_description = models.CharField( max_length=500 , verbose_name="Description" , null=True , blank=True )
    seo_keywords = models.CharField( max_length=200 , verbose_name="Keywords" , null=True , blank=True )
    menutitle = models.CharField(max_length=200, verbose_name="Название в меню", null=True, blank=True)
    menuposition = models.CharField(max_length=200, verbose_name="Позиция в меню", null=True, blank=True)
    menushow = models.BooleanField(default=True, verbose_name=u"Показывать в меню")
    sitemap = models.BooleanField(default=True, verbose_name=u"Показывать в карте сайта")
    og_title = models.CharField(max_length=200, verbose_name="OG Title", null=True, blank=True)
    og_description = models.TextField(max_length=2000, verbose_name="OG Description", null=True, blank=True)
    og_type = models.CharField(max_length=200, verbose_name="OG Type", null=True, blank=True)
    og_image = models.CharField(max_length=500, verbose_name="OG image", null=True, blank=True)
    og_type_pb_time = models.DateField(default=datetime.date.today, verbose_name=u"Время публикации")
    og_type_author = models.CharField(max_length=200, verbose_name="OG author", null=True, blank=True)
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=2, verbose_name=u"Хост")
    
    def save(self, *args, **kwargs):
        if not self.hash:
            self.hash = defaultfilters.slugify(unidecode(self.name))
            
        super(Page, self).save(*args, **kwargs)
    

class Textpage(Page):
    class Meta:
        verbose_name = u"Текстовая страница"
        verbose_name_plural = u"Страницы"
    
    maintitle = models.CharField( max_length=200 , verbose_name=u"Название" )
    menuposition = models.IntegerField(verbose_name=u"Позиция в меню")
    
    def save(self, *args, **kwargs):
        if not self.hash:
            self.hash = defaultfilters.slugify(unidecode(self.maintitle))
            
        super(Textpage, self).save(*args, **kwargs)
    
    def __unicode__(self):
        return self.maintitle
        
class Category(Page):
    class Meta:
        verbose_name = u"Категория"
        verbose_name_plural = u"Категории"
    
    name = models.CharField(max_length=200, verbose_name=u"Название категории")
    comment = models.CharField(max_length=2000, verbose_name=u'Комментарий', null=True ,blank=True)
    domain = models.CharField(max_length=100, verbose_name=u"Домен", null=True ,blank=True)
    background = models.ImageField(verbose_name=u'Задний фон на странице Категории ( 1920x1080 )', null=True , blank=True)
    genre = models.CharField(max_length=100, verbose_name=u"Жанр", null=True ,blank=True)
    language = models.CharField(max_length=100, verbose_name=u"Язык", null=True ,blank=True)
    release = models.CharField(max_length=100, verbose_name=u"Дата выхода", null=True ,blank=True)
    creator = models.CharField(max_length=100, verbose_name=u"Создатель", null=True ,blank=True)
    razrab = models.CharField(max_length=100, verbose_name=u"Разработчик", null=True ,blank=True)
    osob = models.CharField(max_length=100, verbose_name=u"Особенности", null=True ,blank=True)
    youtube = models.CharField(max_length=1000, verbose_name=u"Youtube Ролик", null=True ,blank=True)
    
    def __unicode__(self):
        return self.name

class Subcategory(Page):
    class Meta:
        verbose_name = u"Подкатегория"
        verbose_name_plural = u"Подкатегории"
    
    name = models.CharField(max_length=200, verbose_name=u"Название подкатегории")
    category = models.ForeignKey(Category, verbose_name=u'Основная категория')
    comment = models.CharField(max_length=2000, verbose_name=u'Комментарий', null=True, blank=True)
    
    def __unicode__(self):
        return self.hash

class Item(Page):
    class Meta:
        verbose_name = u"Товар"
        verbose_name_plural = u"Товары"
    
    name = models.CharField(max_length=200, verbose_name=u"Название товара")
    menuposition = models.IntegerField(verbose_name=u'Позиция в меню', null=True , blank=True)
    price = models.FloatField(default=1, verbose_name=u'Цена за единицу товара')
    category = models.ForeignKey(Category, verbose_name=u'Категория', null=True , blank=True)
    min_pay = models.IntegerField(default=1, verbose_name=u'Минимальная количество покупки')
    count = models.IntegerField(default=0, verbose_name=u'Количество товара', null=True , blank=True)
    items = models.TextField( verbose_name="Строки товара", null=True, blank=True)
    description = models.CharField(max_length=1000, verbose_name=u'Описание товара(до 1000 символов)', null=True , blank=True)
    pic = models.ImageField(verbose_name=u'Изображение товара(превью)', null=True , blank=True)
    itemimage = models.ImageField(verbose_name=u'Картинка на странице товара', null=True , blank=True)
    background = models.ImageField(verbose_name=u'Задний фон на странице товара', null=True , blank=True)
    comment = models.CharField(max_length=2000, verbose_name=u'Комментарий', null=True , blank=True)
    autor = models.CharField(max_length=100, verbose_name=u'Автор контента', null=True , blank=True)
    discount = models.IntegerField(verbose_name=u'Скидка', null=True , blank=True)
    video = models.CharField(max_length=1000, verbose_name=u'Видео с Ютюба', null=True , blank=True)
    slider_big = models.ImageField(verbose_name=u'Слайдер большая картинка ( 800x450 )', null=True , blank=True)
    slider_small = models.ImageField(verbose_name=u'Слайдер маленькая картинка ( 400x225 )', null=True , blank=True)
    slider_position = models.IntegerField(verbose_name=u'Позиция в слайдере ( Отдельно у больших и маленьких )', null=True , blank=True)
    
    def discount_price(self):
        if self.discount:
            discount_price = math.ceil(self.price + ((self.price / 100) * self.discount))
        else:
            discount_price = math.ceil(self.price)
        return discount_price
    
    def __unicode__(self):
        return self.hash

class Client(models.Model):
    class Meta:
        verbose_name = u"Клиент"
        verbose_name_plural = u"Клиенты"
    
    mail = models.CharField(max_length=200, verbose_name=u"Ящик клиента")
   
    def __unicode__(self):
        return self.mail
        
class PayMethod(models.Model):
    class Meta:
        verbose_name = u"Метод оплаты"
        verbose_name_plural = u"Методы оплаты"
    
    paymethod = models.CharField(max_length=200, verbose_name=u"Тип Оплаты")
    token = models.CharField(max_length=2000, verbose_name=u"Токен системы")
    phone = models.CharField(max_length=20, verbose_name=u"Номер телефона")
   
    def __unicode__(self):
        return self.paymethod

class Order(models.Model):
    class Meta:
        verbose_name = u"Заказ"
        verbose_name_plural = u"Заказы"
    
    order_id = models.IntegerField( verbose_name=u"Id Заказа")
    hash = models.CharField(max_length = 100, verbose_name=u'Hash заказа', null=True, blank=True)
    paymethod = models.ForeignKey(PayMethod, verbose_name=u"Метод оплаты")
    client = models.ForeignKey(Client, verbose_name=u"Клиент")
    purse = models.CharField(max_length=200, verbose_name=u"Номер Кошелька", null=True, blank=True)
    item = models.ForeignKey(Item, verbose_name=u'Товар')
    count = models.IntegerField(verbose_name=u'Количество купленного товара')
    items = models.TextField( verbose_name="Строки товара", null=True, blank=True)
    status = models.CharField(max_length = 20, verbose_name=u'Статус заказа')
    price = models.FloatField( verbose_name=u'Сумма заказа')
    
    def __unicode__(self):
        return self.hash
        
class ClientsAndOrders(models.Model):
    class Meta:
        verbose_name = u"Клиент и их заказы"
        verbose_name_plural = u"Клиент и их заказы"
    
    client = models.ForeignKey(Client, verbose_name=u"Клиент")
    orders = models.ManyToManyField(Order, verbose_name='Заказы', blank=True)
   
    def __unicode__(self):
        return self.client.mail
        
class CommentAuths(models.Model):
    class Meta:
        verbose_name = u"Авторизированные юзеры(к)"
        verbose_name_plural = u"Авторизированные юзеры(к)"
    
    vkid = models.CharField(max_length = 100, verbose_name=u'id vk')
    username = models.CharField(max_length = 500, verbose_name=u'Имя в вк')
    hash = models.CharField(max_length = 500, verbose_name=u'hash')
    avatar = models.CharField(max_length = 500, verbose_name=u'avatar')
    admin = models.BooleanField(default=False, verbose_name=u"Админ")
   
    def __unicode__(self):
        return self.vkid
        
class CommentVk(models.Model):
    class Meta:
        verbose_name = u"Комментарий вк"
        verbose_name_plural = u"Комментарии вк"
    
    vkid = models.CharField(max_length = 100, verbose_name=u'id vk')
    username = models.CharField(max_length = 500, verbose_name=u'Имя в вк')
    time = models.CharField(max_length = 500, verbose_name=u'Дата коммента')
    comment = models.CharField(max_length = 5000, verbose_name=u'Плохой коментарий')
    news_comment = models.CharField(max_length = 5000, verbose_name=u'Хороший Коментарий', null=True, blank=True)
    avatar = models.CharField(max_length = 500, verbose_name=u'avatar')
    view = models.BooleanField(default=False, verbose_name=u"Показывать?")
    
    def datetime(self):
        time = dict(
            time=datetime.datetime.fromtimestamp(int(self.time)),
            now=datetime.datetime.now()
            )
        return time

    def __unicode__(self):
        return self.vkid
        
class Scripts(models.Model):
    class Meta:
        verbose_name = u"Скрипт"
        verbose_name_plural = u"Импортируемые скрипты"

    name = models.CharField( max_length=200 , verbose_name=u"Заголовок" )
    content = models.TextField( verbose_name=u"Содержимое" )

    def __unicode__(self):
        return self.name
        
class Robots(models.Model):
    class Meta:
        verbose_name = u"Robots"
        verbose_name_plural = u"Robots"

    content = models.TextField(verbose_name=u"Содержимое")
    host = models.ForeignKey(Site, on_delete=models.deletion.SET_DEFAULT, default=2, verbose_name=u"Хост")

    def __unicode__(self):
        return self.content