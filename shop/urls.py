"""desbooks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^clients/$', views.clientspage, name='clientspage'),
    url(r'^categories/(?P<category_alias>[0-9A-Za-z\-]+)/(?P<alias>[0-9A-Za-z\-]+)/$', views.product, name='product'),
    url(r'^categories/$', views.categories, name='categories'),
    url(r'^test/$', views.test, name='test'),
    url(r'^auth/$', views.soc_auth, name='soc_auth'),
    url(r'^reviews/$', views.comments, name='comments'),
    url(r'^agreement/$', views.agreement, name='agreement'),
    url(r'^aktivaciya-klyucha-v-steam/$', views.activate, name='activate'),
    url(r'^aktivaciya-klyucha-v-origin/$', views.activate_o, name='activate_o'),
    url(r'^get/fields/$', views.get_fields, name='get_fields'),
    url(r'^category/(?P<alias>[0-9A-Za-z\-]+)/$', views.category, name='category'),
    url(r'^ajax/$', views.ajax, name='ajax'),
    url(r'^admin/', admin.site.urls),
    url(r'^robots\.txt$', views.robots, name='robots'),
    url(r'^(?P<alias>[0-9A-Za-z\-]+)/$', views.textpage, name='textpage'),
]