# -*- coding: utf-8 -*-
from django.contrib import admin

from shop.models import Textpage, Category, Subcategory, Item, PayMethod, Client, Order, ClientsAndOrders, Scripts, Robots, CommentVk

# Register your models here.
page_fields = [
    (u"Настройки страницы" , {'fields':['menutitle','alias','menushow', 'host','sitemap']}),
    (u"SEO информация" , {'fields':['seo_h1','seo_title','seo_description','seo_keywords','content',]})
]
page_list = ('menushow','sitemap')

#Страницы
class TextpageAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['maintitle', 'menuposition',]}),] + page_fields
    list_filter = ['host']
    def get_prepopulated_fields(self, request, obj=None):
        # can't use `prepopulated_fields = ..` because it breaks the admin validation
        # for translated fields. This is the official django-parler workaround.
        return {"alias": ("maintitle",)}
    
admin.site.register(Textpage, TextpageAdmin)

#Категории
class CategoryAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name','background','genre','youtube','release','language','razrab','osob','creator', 'comment', 'domain']}),] + page_fields
    list_filter = ['host']
    
admin.site.register(Category, CategoryAdmin)

class CommentVkAdmin(admin.ModelAdmin):
    list_display = ('username','vkid','comment', 'news_comment', 'view')
    fieldsets = [(u'Основные', {'fields': ['vkid', 'comment', 'username', 'news_comment', 'view', 'time','avatar']}), ]
    list_editable = ['news_comment', 'view']
#
admin.site.register(CommentVk, CommentVkAdmin)

#Подкатегории
class SubcategoryAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name', 'category', 'comment',]}),] + page_fields
    list_filter = ['host']
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "category":
            kwargs["queryset"] = Category.objects.filter(host__name=request.get_host())
        return super(SubcategoryAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
admin.site.register(Subcategory, SubcategoryAdmin)
#Товары
class ItemAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['name', 'price','menuposition','category', 'min_pay', 'autor', 'count', 'items', 'description', 'pic', 'itemimage', 'video','background','discount', 'comment', 'slider_big', 'slider_small', 'slider_position']}),] + page_fields
    list_filter = ['host']
    list_display = ('name','alias','menuposition', 'category','description','price','discount')
    list_editable = ['alias','price','menuposition','description','category','discount']
    search_fields = ('name','alias')
    
    def get_prepopulated_fields(self, request, obj=None):
        return {"alias": ("name",)}
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "category":
            kwargs["queryset"] = Category.objects.filter(host__name=request.get_host())
        return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    # def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # if db_field.name == "host":
            # kwargs["queryset"] = Site.objects.filter(name=request.get_host())
        # return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    
admin.site.register(Item, ItemAdmin)

class PayMethodAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['paymethod', 'token', 'phone']}),]
    
admin.site.register(PayMethod, PayMethodAdmin)

class ClientAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['mail']}),]
    
admin.site.register(Client, ClientAdmin)

class OrderAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['order_id', 'hash', 'paymethod', 'client', 'purse', 'item', 'count', 'items', 'status', 'price']}),]
    
admin.site.register(Order, OrderAdmin)

class ClientsAndOrdersAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные',  {'fields':['client', 'orders']}),]
    
admin.site.register(ClientsAndOrders, ClientsAndOrdersAdmin)

class ScriptsAdmin(admin.ModelAdmin):
    fieldsets = [(u'Основные', {'fields': ['name', 'content', ]}), ]
#
admin.site.register(Scripts, ScriptsAdmin)

class RobotsAdmin(admin.ModelAdmin):
    fieldsets = [('Содержимое', {'fields': ['content','host']}), ]

admin.site.register(Robots, RobotsAdmin)