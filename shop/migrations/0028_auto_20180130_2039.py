# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-30 20:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0027_auto_20180128_2145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commentvk',
            name='comment',
            field=models.CharField(max_length=5000, verbose_name='\u041f\u043b\u043e\u0445\u043e\u0439 \u043a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439'),
        ),
        migrations.AlterField(
            model_name='commentvk',
            name='news_comment',
            field=models.CharField(blank=True, max_length=5000, null=True, verbose_name='\u0425\u043e\u0440\u043e\u0448\u0438\u0439 \u041a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439'),
        ),
    ]
