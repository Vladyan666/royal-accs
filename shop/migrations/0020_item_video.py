# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-18 23:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0019_auto_20180118_2233'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='video',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='\u0412\u0438\u0434\u0435\u043e \u0441 \u042e\u0442\u044e\u0431\u0430'),
        ),
    ]
