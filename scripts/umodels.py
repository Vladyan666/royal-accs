# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

import os
import sys
from inspect import isclass

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

import django
from django.apps import apps

from updater import ImportData


class ImportTextPages(ImportData):
    def __init__(self, language):
        super(ImportTextPages, self).__init__(language=language)

        self.file_name = 'textdata'
        self.object = apps.get_model('portal', 'TextPage')
        self.title = 'pages'
        self.required = ('id',)
        self.fk_fields = [('host', apps.get_model('sites', 'Site'), 'domain')]


class ImportPlayer(ImportData):
    def __init__(self, language):
        super(ImportPlayer, self).__init__(language)

        self.file_name = 'player'
        self.object = apps.get_model('portal', 'Player')
        self.title = 'player'
        self.required = ('name', 'host')
        self.many = True
        self.fk_fields = [
            ('host', apps.get_model('sites', 'Site'), 'domain'),
            ('team', apps.get_model('portal', 'Team'), 'name')
        ]


RULES = {
    'pages': ImportTextPages,
    'player': ImportPlayer,
}


def init_rules(obj_name):
    if obj_name is None:
        print("Need the model name!")
        return

    return RULES.get(obj_name, None)


if __name__ == '__main__':
    # reload(sys)
    # sys.setdefaultencoding('utf8')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "base.settings")  # указать название своего приложение (к примеру myapp.settings)
    django.setup()

    language = 'ru'
    if len(sys.argv) >= 3:
        language = sys.argv[2] or 'ru'

    title = sys.argv[1]
    callback = init_rules(title)
    if isclass(callback) and issubclass(callback, ImportData):
        instance = callback(language=language)
        instance.run()