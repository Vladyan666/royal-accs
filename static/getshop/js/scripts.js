$(document).ready(function(){
	scroll = function(){
		if( $(window).height() + $('.footer').height() < $('html').height()){
			if($(window).scrollTop() > 22 ){
				$('.top_bar.mainpage').addClass('fixed')
			}
			else{
				$('.top_bar.mainpage').removeClass('fixed')
			}
			
			if( $('div').is('.header_search_block') ){
				if($(window).scrollTop() > $('.header_search_block').offset().top ){
					$('.top_search').addClass('active')
				}
				else{
					$('.top_search').removeClass('active')
				}
			}
			else{
				$('.top_search').addClass('active')			
			}
/*			
			if($(window).scrollTop() > $('.footer').offset().top - $(window).height() ){
				$('.top_bar').addClass('hiddenn')
			}
			else{
				$('.top_bar').removeClass('hiddenn')
			}*/
		}
	}
	$(document).scroll(function(){
		scroll()
	})
	scroll()

	
	var change_temp = "";
	$('.header_search_block input').bind('keydown keyup',function(e){
	    if(e.type == "keyup"){
	        change_temp == $(this).val();
	        $('.clear_search').hide()
	    }
	    if($(this).val() != change_temp){
	        $('.clear_search').css('display','inline-block')
	    }
	
	});
	
	$('.right_checkbox_container').each(function(){
		if( $(this).find('.checkbox_custom').length < 6 ){
			$(this).next().hide()
		}
	})
    $('.choose_rating svg').hover(function(){
	    $(this).prevAll('svg').attr('class','active')
    },function(){
	    $(this).prevAll('svg').attr('class','')
    })
    $('.add_cont_steps__five li, .add_cont_steps li').click(function(){
		$(this).prevAll().css('color','#fff');$(this).nextAll().css('color','#aeaeae'); $(this).css('color','#fff')
    })
    if($(window).width() < 769){
	    $('.production_list_wrap').each(function(){
		    $(this).css('width', $(this).find('.production_item_container').length * 250)
	    })
	    $('.infobussinesmans_list_wrap').each(function(){
		    $(this).css('width', $(this).find('.infobussinesmans_item').length * 270)
	    })
    }

    $('body .mobile_menu_call').click(function(){
    	if(!$(this).hasClass('active')){
			$('.mobile_menu_inner').fadeIn(300); 
			$('#wrapper').css('filter','blur(6px)'); 
			$('body').css('overflow','hidden');
			$('.top_bar').addClass('mobile_menu_opened');
			$(this).addClass('active');
    	}
    	else{
	    	$('.mobile_menu_inner').fadeOut(300);
	    	$('#wrapper').css('filter','blur(0px)'); 
	    	$('body').css('overflow','auto');
	    	$('.top_bar').removeClass('mobile_menu_opened');
	    	$(this).removeClass('active')
    	}
    })
})